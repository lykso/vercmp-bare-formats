#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_dotless_patch() {
    # a semver that sometimes uses letters as an additional patch field without
    # bothering to add a dot.
    comparever_default \
        "$(echo "$1" | sed 's/\([a-z]\+[0-9a-z]*\)$/.\1/')" \
        "$(echo "$2" | sed 's/\([a-z]\+[0-9a-z]*\)$/.\1/')"
}
comparever() { comparever_dotless_patch $@; }
