#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_default_with_underscores() {
    comparever_default "$(echo "$1" | tr '_' '.')" "$(echo "$2" | tr '_' '.')"
}
comparever() { comparever_default_with_underscores $@; }
