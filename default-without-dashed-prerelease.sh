#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_gdb() {
    # a semver that eschews using a dash before prerelease versions.
    comparever_default "$(echo "$1" | sed 's/\([a-z]\+[0-9a-z]*\)$/-\1/i')" \
                       "$(echo "$2" | sed 's/\([a-z]\+[0-9a-z]*\)$/-\1/i')"
}
comparever() { comparever_gdb $@; }
